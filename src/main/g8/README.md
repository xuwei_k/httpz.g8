# $name$

[![Build Status](https://secure.travis-ci.org/$githubUser$/$name$.png)](http://travis-ci.org/$githubUser$/$name$)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/$organization$/$name$_2.11/badge.svg)](https://maven-badges.herokuapp.com/maven-central/$organization$/$name$_2.11)
[![Scaladoc](http://javadoc-badge.appspot.com/$organization$/$name$_2.11.svg?label=scaladoc)](http://javadoc-badge.appspot.com/$organization$/$name$_2.11)

- [Maven Central](http://repo1.maven.org/maven2/$organization$/)

### latest stable version

```scala
libraryDependencies += "$organization$" %% "$name$" % "0.1.0"
```

### snapshot version

```scala
resolvers += Opts.resolver.sonatypeSnapshots

libraryDependencies += "$organization$" %% "$name$" % "0.1.1-SNAPSHOT"
```
