package $packageName$

import httpz.{Core, Request}
import scalaz.IList

private[$packageName$] object Interpreter extends CommandToAction {
  override def apply[A](fa: Command[A]) = fa match {
    case _ =>
      null
  }
   
  private def params(p: (String, Option[String]) *): Map[String, String] = {
    p.foldLeft(Map.empty[String, String]){
      case (map, (_, None)) => map
      case (map, (k, Some(v))) => map.updated(k, v)
    }
  }
}
