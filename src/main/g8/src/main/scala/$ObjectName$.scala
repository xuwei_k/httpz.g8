package $packageName$

import httpz.Action
import scalaz.Free.FreeC
import scalaz._

object $ObjectName$ extends $ObjectName$[Command] {
  implicit def instance[F[_]](implicit I: Inject[Command, F]): $ObjectName$[F] =
    new $ObjectName$[F]

  def commands2action[A](a: Commands[A]): Action[A] =
    Free.runFC[Command, Action, A](a)(interpreter)(httpz.ActionMonad)
}


class $ObjectName$[F[_]](implicit I: Inject[Command, F]) {

  private[this] type FreeCF[A] = FreeC[F, A]

  final def lift[A](f: Command[A]): FreeCF[A] =
    Free.liftFC(I.inj(f))
}
